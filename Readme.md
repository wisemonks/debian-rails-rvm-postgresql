# Rails aplikacijos paruošimas tusčiame Linux Debian Wheezy serveryje #

Šio gido pagalba savarankiškai paruošite serverį Rails aplikacijai.

Pastabos:


```
#!bash
remote$ - shell komanda vykdoma serveryje su root pagalba
remote.fundamentis$ - shell komanda vykdoma serveryje su darbinio vartotoja pagalba
local$ - shell komanda vykdoma vietiniame kompiuteryje

```



## 1. Jei žinome tik IP, tai patogu jį pakeisti lengviau atsimenu pavadinimu pvz. fundamentis.app ##


```
#!bash

local$ sudo gedit /etc/hosts
```


Ir paskutinėje eilute pridedam:

```
#!bash

185.5.52.156 fundamentis.app
```


Dabar galime jungtis prie serverio

```
#!bash

local$ ssh root@fundamentis.app
```


## 2. Jei nenorime, kad kiekviena karta jungiantis mūsų prašytu slaptažodžio ##

```
#!bash

local$ ssh-add -L
```


Nusikopijuojam vieną iš privačių raktų.


```
#!bash

remote$ echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCybbe7ZQd+8GMh1ZXhmwY/rVvFA25R6P4U4ZjiXAS6CKRalGnG1HInE0/0xmw34JJzLv33le7dD/vL7r8N+wFldrscykyZqk3Br+Yxi/lr5KTHEPckd6nehy8RM3FHw7Osz4ZM2YOrtuiNYHDtIgMKjmPYwWQh88gSMVoiXWeOCXq7NAcWMBY/OiSWg/K0UEWsZWcqcssiKR9N2E3U2QkQq5kzL03QXNtW9ejP2ebDfNZcBenmHMn/dDHUHUgW1ANzqyDHX1et3NUKX5ySfEkZQqGGp32SZoWcMpl4Vjp+tG4N+ypnnHreF/oY4BENmWWwCAmiQD91xU7HT76wmmkd arturas@wisemonks.com" >> .ssh/authorized_keys
```


Jei nėra /root/.ssh/authorized_keys failo ir direktorijos, tai juos reiktų sukurti.
Direktorijai uždėkite 0700, o failui 0600.

## 3. Prisijungus atnaujinam apt repozitorijas ##


```
#!bash

remote$ apt-get update && apt-get upgrade
```


Jei tarp įdiegtų paketų matote pvz. linux-headers-* paketą, tai labai sveika būtų perkrauti serverį:

```
#!bash

remote$ reboot
```


## 4. Kai kuriuose Debian distribucijoje apache yra default webserveri. Išmetam jį: ##


```
#!bash

apt-get remove apache2 apache2-doc apache2-mpm-prefork apache2-utils apache2.2-bin apache2.2-common
```


## 5. Diegiam pagrindinius defaultinius paketus, kurie yra bazė ir leis toliau diegti kitas programas. ##


```
#!bash

apt-get install wget build-essential zlib1g-dev libssl-dev libpcre3-dev libcurl4-openssl-dev curl git-core imagemagick libreadline6-dev libyaml-dev libsqlite3-dev sqlite3 autoconf libgdbm-dev libncurses5-dev automake libtool bison libffi-dev sudo libpq-dev gawk
```

### 5.1 Diegiame nodejs, kuris bus naudojamas assets::precompile'ams. ###

[https://nodejs.org/en/download/package-manager/](https://nodejs.org/en/download/package-manager/)

```
#!bash
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
apt-get install nodejs'
```


## 6. Diegiam postgresql ##

Kartais postgresql nesugeba pilnai įdiegti dėl nesukonfiguruotų lokalių
nustatymų. Galima pabandyti iš naujo sugeneruoti lokales:

```
#!bash

remote$  dpkg-reconfigure locales
```

Jeigu matote error'ą, kuriame yra kažkas panašaus į
```
#!bold
LC_ALL = (unset)
```
Reikėtų paleisti:

```
#!bash
remote$ echo 'LC_ALL="en_GB.utf8"' >> /etc/enviroment
```


Jei reikalinga naujausia versija, tai ją reiktų diegti su papildomis instrukcijomis:
[http://www.pontikis.net/blog/postgresql-9-debian-7-wheezy](http://www.pontikis.net/blog/postgresql-9-debian-7-wheezy)
Šiam kartui užteks default versijos


```
#!bash

remote$ apt-get install postgresql postgresql-client
```


### 6.1. Sukuriam DB vartotoją savo aplikacijai ###


```
#!bash

remote$ su - postgres
remote.postgres$ psql
postgres=# CREATE USER fundamentis WITH PASSWORD '85e41w1q45rf';
postgres=# CREATE DATABASE fundamentis_production OWNER fundamentis;
postgres=# \q
```


## 7. Sukuriam vartotoją aplikacijai ##


```
#!bash

remote$ adduser fundamentis
```


Slaptažodį uždėkite sudėtingesni, bet tokį, kad atsimintume. Vėliau jį reikės pakeisti.

## 8. Vartotojui laikinai suteikiam sudo teisės. ##


```
#!bash

remote$ apt-get install sudo
remote$ visudo
```


Žemiau root eilutės įrašome:


```
#!bash

fundamentis ALL=(ALL:ALL) ALL
```


### 8.1. Prisijungę prie naujo vartotojo atliekame tą patį kaip 2 punkte. ###


## 9. Diegiame RVM. ##


```
#!bash

remote$ su fundamentis
remote.fundamentis$ curl -sSL https://get.rvm.io | bash -s stable --ruby
```


Tikėtina, kad jūsų prašys slaptažodžio papildomų apt paketų įdiegimui.

## 10. Tam, kad kiekviena karta prisijungus prie vartotojo automatiškai užsiloadintu rvm su default versija į .bashrc failo galą įrašome: ##


```
#!bash

source /home/fundamentis/.rvm/scripts/rvm
```


## 11. Diegiame passenger'į. ##


```
#!bash

remote.fundamentis$ gem install passenger
remote.fundamentis$ rvmsudo passenger-install-nginx-module --extra-configure-flags="--with-http_gzip_static_module --with-http_ssl_module --with-http_v2_module"

```

Diegimo metu visur spauskite enter t.y. default pasirinkimai.

Jeigu serveryje yra mažiau negu 1GB RAM - passenger'io install'as gali strigti. Tuomet reikia paleisti
```
#!bash

#Sukuriama 1GB SWAP 
sudo dd if=/dev/zero of=/swap bs=1M count=1024
sudo mkswap /swap
sudo swapon /swap
```

Po diegimo gausite konfiguracinius kelius iki passengerio ir ruby. Juos nusikopijuokite.

```
#!bash

passenger_root /home/fundamentis/.rvm/gems/ruby-2.1.3/gems/passenger-4.0.53;
passenger_ruby /home/fundamentis/.rvm/gems/ruby-2.1.3/wrappers/ruby;
```

Šias eilutes įrašykite į šioje repozitorijoje esantį failą: nginx/nginx.conf

Nepamirškite sužiūrėti kitų reikšmių nginx.conf faile.

Šį failą patalpinkite serveryje:


```
#!bash

/opt/nginx/conf/nginx.conf

```

Patikriname ar nepadarėme klaidų:


```
#!bash

remote$ /opt/nginx/sbin/nginx -t
```

### 11.1 Nginx start-up skriptas ###

Repozitorijoje nginx/nginx failą talpiname serveryje


```
#!bash

/etc/init.d/nginx
```

Keičiame teisės ir padarome jį automatiškai startuojamu:


```
#!bash

remote$ chmod +x /etc/init.d/nginx
remote$ /usr/sbin/update-rc.d -f nginx defaults
```

Startuojam nginx:

```
#!bash

remote$ /etc/init.d/nginx start
```

## 12. Kol diegia nginx'a su passengeriu, savo aplikacijoje įdiegiam Capistrano. ##
Gemfile įrašom:


```
#!ruby

gem 'capistrano-rails', group: :development
gem 'capistrano-rvm', group: :development

```


```
#!bash

local$ bundle update
local$ cap install
```

Koreguojame Capfile (pvz. rasite repozitorijoje app/Capfile)

Koreguojame config/deploy.rb (pvz. rasite repozitorijoje app/config/deploy.rb)

Koreguojame config/deploy/production.rb (pvz. rasite repozitorijoje app/config/deploy/production.rb)

Pabandom pratestuoti ar viską teisingai sukonfiguravom:


```
#!bash

local$ cap production deploy:check
```

Gausite klaida, kad nėra database.yml, tad sukuriam jį serveryje:


```
#!bash

remote.fundamentis$ /home/fundamentis/shared/config/database.yml

```

Pratestuojame dar kartą. Jei viskas ok, tai visus pakeitimus commit'inam ir push'inam.

Deploy'inam:


```
#!bash

local$ cap production deploy

```

Galima išvengti `production` rašymo `Capfile` nurodžius default environment .  `Capfile` apačioje užrašykite:

```ruby

invoke :production

```

Jeigu norite prisijungti prie serverio ir leisti rake, rails komandas reikėtų iš deploy.rb failo :linked_dirs panaikinti bin direktoriją, kad būtų kažkas panašaus į:

```ruby
set :linked_dirs, %w{log tmp/pids tmp/cache}
```
o ne
```ruby
set :linked_dirs, %w{bin log tmp/pids tmp/cache}
```
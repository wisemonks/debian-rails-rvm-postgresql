#!/usr/bin/env puma

directory '/home/poppri/current'
rackup "/home/poppri/current/config.ru"
environment 'production'

pidfile "/home/poppri/shared/tmp/pids/puma.pid"
state_path "/home/poppri/shared/tmp/pids/puma.state"
stdout_redirect '/home/poppri/shared/log/puma_access.log', '/home/poppri/shared/log/puma_error.log', true


threads 0,16

bind 'unix:///home/poppri/shared/tmp/sockets/puma.sock'

workers 2



prune_bundler


on_restart do
  puts 'Refreshing Gemfile'
  ENV["BUNDLE_GEMFILE"] = "/home/poppri/current/Gemfile"
end
